#include <iostream>
#include <string>
#include <vector>
#include <functional>
#include <thread>
#include <fstream>
#include <iomanip>
// RADAR SDK 
#include <radarsdk.h>

using namespace std;

void doSomething() {
    std::this_thread::sleep_for(chrono::seconds(5));
}

auto frameDone = [](uint64_t TimeStamp, uint64_t GrabNumber, int64_t Status, double Speed, vector<TCogRadarTarget>& targets, void* arg) {
    cout << " FRAME:" << setw(5) << setfill('0') << GrabNumber;
    if (targets.size() == 0) {
        cout << " [ empty ]";
    }
    for (TCogRadarTarget tg : targets) {
        cout << " [" << fixed << setprecision(1) << tg.Range << ", " << tg.Azimuth << "]";
    }
    cout << endl;
};


int main() {
    
    //TCogRadarX cprr("cprr");
    TCogRadarX cprr("demo");
    
    cout << " RADAR CONNECTING..." << endl;

    if (cprr.open(TCogParser::configFile2Str("minimal.cfg"))) {
        cout << " RADAR OK! " << endl;
        cout << " Version Hardware: " << cprr.getVersion().HardVersion << \
                " Version Software: " << cprr.getVersion().SoftVersion << \
                " SN : " << cprr.getVersion().SerialNumb << endl;

        cout << " RADAR START " << endl;
        cprr.start("STREAM", frameDone, NULL);
        // ......
        doSomething();
        // ......
        cout << " RADAR STOP " << endl;
        cprr.stop();
        cout << " RADAR CLOSE " << endl;
        cprr.close();
    } else {
        cout << " RADAR ERROR! " << endl;
    }

    cout << " DONE " << endl;
    return 0;
}