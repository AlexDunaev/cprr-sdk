#ifndef _COG_UTILS_
#define _COG_UTILS_


#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <algorithm>
using namespace std;

class TCogParser {
public:
    //"Key1=Value1; Key2=Value2" --> map<string(toupper), string>

    static std::map<std::string, std::string> getParams(const string& paramLine) {
        std::map<std::string, std::string> result;
        stringstream paramAsStream(paramLine);
        string subLine;
        while (paramAsStream >> subLine) {
            for (const std::string& tag : split(subLine, ";")) {
                auto key_val = split(tag, "=");
                if (key_val.size() == 2) {
                    std::for_each(key_val[0].begin(), key_val[0].end(), [](char & c) {
                        c = ::toupper(c); });
                    result.insert(std::make_pair(key_val[0], key_val[1]));
                }
            }
        }
        return result;
    }

    static std::string configFile2Str(string cfgFileName) {
        std::stringstream strStream;
        std::ifstream inFile;
        inFile.open(cfgFileName);
        strStream << inFile.rdbuf();
        return strStream.str();
    }
private:
    // Disallow creating an instance of this object

    TCogParser() {
    }

    static vector<string> split(const string& str, const string& delim) {
        vector<string> tokens;
        size_t prev = 0, pos = 0;
        do {
            pos = str.find(delim, prev);
            if (pos == string::npos) pos = str.length();
            string token = str.substr(prev, pos - prev);
            if (!token.empty()) tokens.push_back(token);
            prev = pos + delim.length();
        } while (pos < str.length() && prev < str.length());
        return tokens;
    }
};

#endif

