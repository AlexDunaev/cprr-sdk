#ifndef _COG_PROTOCOL_
#define _COG_PROTOCOL_

#include <string>
#include <sstream>
#include <iostream>
#include <functional>


class TCogProtocolFrame {
public:
    virtual int unpack(std::istream& in, std::function<void (void *, unsigned int) > frameEvent) = 0;
};

class TCogProtocol {
public:
    virtual std::stringstream& pack(void * buffer, unsigned int size) = 0;
    //virtual int  unpack(void * buffer, unsigned int size) = 0;
};

#endif