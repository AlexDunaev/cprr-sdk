
#ifndef _COG_DATAPROVIDER_
#define _COG_DATAPROVIDER_

#include <string>
#include <functional>
#include <sstream>
#include <iostream>

typedef std::function<void (void * buffer, unsigned int size)> TCogGetBufferEvent;

class TCogDataProvider {
public:
    virtual bool init(std::string initString) = 0;
    virtual int  write(void * buffer, unsigned int size) = 0;
    virtual int  write(std::istream& stream) = 0;
    virtual bool open(TCogGetBufferEvent rxEvent) = 0;
    virtual bool close() = 0;
};

#endif

