
#ifndef _COG_RADAR_
#define _COG_RADAR_

#include <string>
#include <functional>
#include <vector>
#include "cogDataProvider.h"

struct TCogRadarInfo {
    std::string HardVersion;
    std::string SoftVersion;
    std::string SerialNumb;
};

struct TCogRadarSetPlatform {
    double velocity;
    double yaw_rate;
    int    direction;
};

typedef struct {
    int64_t ObjID;
    double Range;
    double Azimuth;
    int64_t LiveTime;
    double Rcs;
    double Xcoord;
    double Xrate;
    double Xacceleration;
    double Ycoord;
    double Yrate;
    double Yacceleration;
} TCogRadarTarget;

typedef std::function<void (uint64_t TimeStamp, uint64_t GrabNumber, int64_t Status, double Speed, std::vector<TCogRadarTarget>&, void * arg) > TCogFrameEvent;

class TCogRadar {
public:
    virtual ~TCogRadar() {};
    virtual bool open(std::string initString) = 0;
    virtual bool close() = 0;
    virtual bool start(std::string modeString, TCogFrameEvent frameDoneEvent, void * arg) = 0;
    virtual bool stop() = 0;
    virtual TCogRadarInfo & getVersion() = 0;
    virtual bool setPlatformInfo(TCogRadarSetPlatform info) = 0;
    virtual bool set(std::string cmdString) = 0;
    virtual std::string get(std::string cmdString) = 0;
    virtual bool setDataProvider(TCogDataProvider* dataProvider) = 0;
};


#endif