
#ifndef _COG_NULLDATAPROVIDER_
#define _COG_NULLDATAPROVIDER_

#include <string>
#include "../cogDataProvider.h"

class TCogNullDataProvider : public TCogDataProvider {
public:
    TCogNullDataProvider() {
    }
    ~TCogNullDataProvider() {
    }
    bool init(std::string initString) {
        return true;
    };
    bool open() {
        return true;
    }
    bool close() {
        return true;
    }
};


#endif

