#ifndef DATAGRAMSOCKET_H_INCLUDED
#define DATAGRAMSOCKET_H_INCLUDED

#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE


#ifdef WIN32
#pragma comment(lib, "Ws2_32.lib")
#include <winsock2.h>
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#endif

#define STRCPY_MAX_NUM  255


#ifdef WIN32
typedef int socklen_t;
#endif

class DatagramSocket {
private:
#ifdef WIN32
    WSAData wsaData;
    SOCKET sock;
#else
    int sock;
#endif
    long retval;
    sockaddr_in outaddr;
    char ip[STRCPY_MAX_NUM];
    char received[STRCPY_MAX_NUM];
    int sockClosed;

    //    void DatagramShowDump(const char* msg, int msgsize) {
    //        std::cout << "[" << msgsize << "] ";
    //        for (int i = 0; i < msgsize; i++) {
    //            unsigned char h = msg[i];
    //            std::cout << std::hex << std::setw(2) << std::setfill('0') << (int) h << " ";
    //        }
    //        std::cout << std::dec << std::endl;
    //    }

    void strcpy_s(char* dst, size_t size, const char* src) {
        if (!dst || !src) return;
        for (; size > 0; --size) {
            if (!(*dst++ = *src++)) return;
        }
    }

public:

    DatagramSocket(int port_dest, int port_src, const char* address, bool broadcast, bool reusesock) {

#ifdef WIN32
        retval = WSAStartup(MAKEWORD(2, 2), &wsaData);
#endif

        sockaddr_in addr;
        sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

        sockClosed = 0;
        //set up bind address
        memset(&addr, 0, sizeof (addr));
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = htonl(INADDR_ANY);
        addr.sin_port = htons(port_src);

        //set up address to use for sending
        memset(&outaddr, 0, sizeof (outaddr));
        outaddr.sin_family = AF_INET;
        outaddr.sin_addr.s_addr = inet_addr(address);
        outaddr.sin_port = htons(port_dest);

#ifdef WIN32
        bool bOptVal = 1;
        int bOptLen = sizeof (bool);
#else
        int OptVal = 1;
#endif

        if (broadcast)
#ifdef WIN32
            retval = setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (char*) &bOptVal, bOptLen);
#else
            retval = setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &OptVal, sizeof (OptVal));
#endif

        if (reusesock)
#ifdef WIN32
            retval = setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char*) &bOptVal, bOptLen);
#else
            retval = setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &OptVal, sizeof (OptVal));
#endif

        retval = bind(sock, (struct sockaddr *) &addr, sizeof (addr));

    }

    ~DatagramSocket() {
        if (sockClosed == 0) {
            SocketClose();
        }
    }

    void SocketClose() {
#if WIN32
        closesocket(sock);
        WSACleanup();
#else
        shutdown(sock, SHUT_RD);
        close(sock);
#endif
        sockClosed = 1;
    }

    int getAddress(const char * name, char * addr) {
        struct hostent *hp;
        if ((hp = gethostbyname(name)) == NULL) return (0);
        strcpy_s(addr, STRCPY_MAX_NUM, inet_ntoa(*(struct in_addr*) (hp->h_addr)));
        return (1);
    }

    const char* getAddress(const char * name) {
        struct hostent *hp;
        if ((hp = gethostbyname(name)) == NULL) return (0);
        strcpy_s(ip, STRCPY_MAX_NUM, inet_ntoa(*(struct in_addr*) (hp->h_addr)));
        return ip;

    }

    long receive(char* msg, int msgsize) {
        if (sockClosed) return 0;
        struct sockaddr_in sender;
        socklen_t sendersize = sizeof (sender);
        int retval = recvfrom(sock, msg, msgsize, 0, (struct sockaddr *) &sender, &sendersize);
        strcpy_s(received, STRCPY_MAX_NUM, inet_ntoa(sender.sin_addr));
        return retval;
    }
#undef STRCPY_MAX_NUM

    char* received_from() {
        return received;
    }

    long send(const char* msg, int msgsize) {
        return sendto(sock, msg, msgsize, 0, (struct sockaddr *) &outaddr, sizeof (outaddr));
    }

    long sendTo(const char* msg, int msgsize, const char* addr) {
        outaddr.sin_addr.s_addr = inet_addr(addr);
        return sendto(sock, msg, msgsize, 0, (struct sockaddr *) &outaddr, sizeof (outaddr));
    }
};


#endif // DATAGRAMSOCKET_H_INCLUDED
