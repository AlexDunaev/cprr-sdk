
#ifndef RADAR_V2_H
#define RADAR_V2_H
#include <stdint.h>
#ifdef __cplusplus
extern "C" {
#endif

   
// Преамбула данного сервера
#define RADAR_PKT_PREAMBLE              0x0000ABCD
   
// Возможные команды сервера
#define RADAR_PKT_REQUEST_ID            1
#define RADAR_PKT_MODE_ID               2
#define RADAR_PKT_PLATFORM_ID           3
#define RADAR_PKT_DATA_ID               4
#define RADAR_PKT_INFO_ID               5
#define RADAR_PKT_ERROR_ID              6
#define RADAR_PKT_MIN_ID                1
#define RADAR_PKT_MAX_ID                6

// Коды запроса в сообщении REQUEST
#define RADAR_REQ_GETVER                1

#define RADAR_PKT_TARGET_MAX_NUM        16

// Типы данных согласно ПИВ

#pragma pack(push, 1)

#define GCC_PACK_ATTR   //__attribute__((__packed__)) 

typedef struct GCC_PACK_ATTR {
    // Преамбула
    uint32_t preamble;
    // Длинна поля данных
    uint32_t length;
    // Команда
    uint32_t pktType;
} TRadarHeaderMessage;

typedef struct GCC_PACK_ATTR {
    uint32_t power;
    uint32_t streaming;
} TRadarPackMode;


typedef struct GCC_PACK_ATTR {
    double   Velocity;
    double   Yaw_rate;
    uint64_t Forward;
} TRadarPackPlatform;

typedef struct GCC_PACK_ATTR {
    uint32_t Error;
} TRadarPackError;

typedef struct GCC_PACK_ATTR {
    int64_t ObjID;
    double Range;
    double Azimuth;
    int64_t LiveTime;
    double Rcs;
    double Xoord;
    double Xrate;
    double Xacceleration;
    double Ycoord;
    double Yrate;
    double Yacceleration;
} TRadarTarget;

typedef struct GCC_PACK_ATTR {
    uint32_t Status;
    uint64_t GrabNumber;
    uint64_t TimeStamp;
    double   Speed;
} TRadarTargetParam;

typedef struct GCC_PACK_ATTR {
    uint32_t Status;
    uint64_t GrabNumber;
    uint64_t TimeStamp;
    double   Speed;
    TRadarTarget Targets [RADAR_PKT_TARGET_MAX_NUM];
} TRadarPackData;

typedef struct GCC_PACK_ATTR {
    uint32_t HardVersion;
    uint32_t SoftVersion;
    uint32_t SerialNumb;
} TRadarPackInfo;


typedef union GCC_PACK_ATTR {
    uint32_t                 request; // Запрос (пока, только версии)
    TRadarPackMode           mode;    // режим питания и выдачи
    TRadarPackPlatform       platform;// параметры платформы.
    TRadarPackInfo           verInfo; // версия прошивки
    TRadarPackData           data;    // данные отметок
    TRadarPackError          error;   // код(ы) ошибок
    char                     buffer[1];//буфер в сыром режиме
} TRadarCmdList;

typedef struct GCC_PACK_ATTR {
    TRadarHeaderMessage   hdr;
    TRadarCmdList         message;
} TRadarPacket;

#pragma pack(pop)

#ifdef __cplusplus
}
#endif

#endif /* RADAR_V2_H */

