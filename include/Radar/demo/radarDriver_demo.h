#ifndef _COG_RADAR_DEMO_
#define _COG_RADAR_DEMO_
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <thread>
#include <chrono>
#include <functional>
#include <cmath>

#include <cogRadar.h>
#include <cogDataProvider.h>
#include <cogProtocol.h>

#define DEMO_TARGET_NUM 40
#define DEMO_TARGET_FPS 10


#define DEMO_DELAY_MS   (1000 / DEMO_TARGET_FPS)

class TCogRadarDemo : public TCogRadar {
private:
    TCogRadarInfo radarInfo = {"0.1", "0.9", "12345"};
    bool radarUp = false;
    bool radarStart = false;
    TCogFrameEvent userFrameDoneEvent = NULL;
    std::vector<TCogRadarTarget> radarTargets;
    std::thread * radarThread;
    bool doExit;
    void * userArg = NULL;
    uint64_t TimeStamp = 0;
    uint64_t GrabNumber = 0;
public:

    TCogRadarDemo() {
    };

    ~TCogRadarDemo() {
    };

    TCogRadarInfo & getVersion() {
        return radarInfo;
    }

    bool setPlatformInfo(TCogRadarSetPlatform info) {
        return true;
    }

    bool start(std::string modeString, TCogFrameEvent frameDoneEvent, void * arg) {
        userArg = arg;
        userFrameDoneEvent = frameDoneEvent;
        radarStart = true;
        return true;
    }

    bool stop() {
        radarStart = false;
        return true;
    }

    bool set(std::string cmdString) {
        return true;
    }

    std::string get(std::string cmdString) {
        return "";
    }

    bool setDataProvider(TCogDataProvider* dataProvider) {
        return true;
    };

    bool open() {
        return open("");
    };

    bool open(std::string initString) {
        radarUp = true;
        radarThread = new std::thread([this] {
            this->readingLoop(); });
        return true;
    }

    bool close() {
        radarUp = false;
        doExit = 1;
        return true;
    }

    void rxEvent(void * buffer, unsigned int size) {
    }

    void rxEvent(std::istream& in) {
    }

private:

    bool readingLoop() {
        doExit = 0;
        while (doExit == 0) {
            std::this_thread::sleep_for(std::chrono::milliseconds(DEMO_DELAY_MS));
            TimeStamp += DEMO_DELAY_MS * 1000;
            GrabNumber++;
            if (radarStart && radarUp) {
                getTarget(DEMO_TARGET_NUM);
                if (userFrameDoneEvent) {
                    userFrameDoneEvent(TimeStamp, GrabNumber, 1, 0.0, radarTargets, userArg);
                }
            }
        }
        return true;
    }


    void targetGeneratorCircle(int indexOfTarget, int numTarget, int id, TCogRadarTarget& target) {
        static int time = 0;
        static double dAngle = 0.0;
        double R = 30.0;
        double L = 40.0;
        if (indexOfTarget == 0) {
            dAngle += 0.1;
            time++;
        }
        double a = dAngle + ((float)indexOfTarget / (float)numTarget) * 3.1415926535 * 2.0;
        double x = R * cos(a);
        double y = R * sin(a) + L;
        target.ObjID = id;
        target.Xcoord = x;
        target.Ycoord = y;
        target.Rcs = 0.001 + 0.0001 * (float)indexOfTarget;
        target.Xacceleration = 0;
        target.Yacceleration = 0;
        target.Xrate = 0;
        target.Yrate = 0;
        target.LiveTime = time;
        target.Range = sqrt(x * x + y * y);
        target.Azimuth = atan(x / y) * 180.0 / 3.1415926535;
    }

    void getTarget(unsigned int targetNum) {
        static int64_t LiveTime = 0;
        LiveTime += DEMO_DELAY_MS * 1000;
        radarTargets.resize(targetNum);
        int tCount = 0;
        for (auto& Target : radarTargets) {
            targetGeneratorCircle(tCount, targetNum, tCount + 1, Target);
            Target.LiveTime = LiveTime;
            tCount++;
        }
    }
};


#endif
