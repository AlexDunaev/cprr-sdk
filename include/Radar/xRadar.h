#ifndef _COG_RADAR_X_
#define _COG_RADAR_X_
#include <string>
#include <iostream>
#include <sstream>
#include <vector>

#include <cogRadar.h>
#include <cogDataProvider.h>
#include <cogProtocol.h>

#include "../Radar/cprr/radarDriver_cprr.h"
#include "../Radar/demo/radarDriver_demo.h"

#define COG_RADAR_CPRR  "cprr"  // CPRR 24GGz
#define COG_RADAR_DEMO  "demo"  // Emulator(demo)


class TCogRadarX : public TCogRadar {
private:
    TCogRadar * radar = NULL;
    TCogRadarInfo radarInfo = {"undef", "undef", "undef"};
public:

    TCogRadarX(std::string radarCode) {
        if (radarCode == COG_RADAR_CPRR) {
            radar = new TCogRadarCPRR();
        }
        if (radarCode == COG_RADAR_DEMO) {
            radar = new TCogRadarDemo();
        }
    };

    ~TCogRadarX() {
        if (radar) {
            delete radar;
        }
    };
    
    TCogRadarInfo & getVersion() {
        if (radar) {
            return radar->getVersion();
        } else {
            return radarInfo;
        }
    }

    bool setPlatformInfo(TCogRadarSetPlatform info) {
        if (radar) {
            return radar->setPlatformInfo(info);
        } else {
            return false;
        }
    }

    bool start(std::string modeString, TCogFrameEvent frameDoneEvent, void * arg) {
        if (radar) {
            return radar->start(modeString, frameDoneEvent, arg);
        } else {
            return false;
        }
    }

    bool stop() {
        if (radar) {
            return radar->stop();
        } else {
            return false;
        }
    }

    bool set(std::string cmdString) {
        if (radar) {
            return radar->set(cmdString);
        } else {
            return false;
        }
    }

    std::string get(std::string cmdString) {
        if (radar) {
            return radar->get(cmdString);
        } else {
            return "";
        }
    }

    bool setDataProvider(TCogDataProvider* dataProvider) {
        if (radar) {
            return radar->setDataProvider(dataProvider);
        } else {
            return false;
        }
    };

    bool open(std::string initString) {
        if (radar) {
            return radar->open(initString);
        } else {
            return false;
        }
    }
    
    bool close() {
        if (radar) {
            return radar->close();
        } else {
            return false;
        }
    }
};


#endif
